export function getFieldsScore(fieldsCount) {
    let fieldsScore;     
    if (fieldsCount <= 1) { fieldsScore = -1; }
    else if (fieldsCount === 2) { fieldsScore = 1; }
    else if (fieldsCount === 3) { fieldsScore = 2; }
    else if (fieldsCount === 4) { fieldsScore = 3; }
    else if (fieldsCount >= 5) { fieldsScore = 4; }
    return fieldsScore;
  }

  export function getPasturesScore(pasturesCount) {
    let pasturesScore;    
    if (pasturesCount === 0) { pasturesScore = -1; }
    else if (pasturesCount === 1) { pasturesScore = 1; }
    else if (pasturesCount === 2) { pasturesScore = 2; }
    else if (pasturesCount === 3) { pasturesScore = 3; }
    else if (pasturesCount >= 4) { pasturesScore = 4; }
    return pasturesScore;
  }

  export function getGrainScore(grainCount) {
    let grainScore;    
    if (grainCount === 0) { grainScore = -1; }
    else if (grainCount <= 3) { grainScore = 1; }
    else if (grainCount <= 5) { grainScore = 2; }
    else if (grainCount <= 7) { grainScore = 3; }
    else if (grainCount >= 8) { grainScore = 4; }
    return grainScore;
  }

  export function getVegetablesScore(vegetablesCount) {
    let vegetablesScore;    
    if (vegetablesCount === 0) { vegetablesScore = -1; }
    else if (vegetablesCount === 1) { vegetablesScore = 1; }
    else if (vegetablesCount === 2) { vegetablesScore = 2; }
    else if (vegetablesCount === 3) { vegetablesScore = 3; }
    else if (vegetablesCount >= 4) { vegetablesScore = 4; }
    return vegetablesScore;
  }

  export function getSheepScore(sheepCount) {
    let sheepScore;    
    if (sheepCount === 0) { sheepScore = -1; }
    else if (sheepCount <= 3) { sheepScore = 1; }
    else if (sheepCount <= 5) { sheepScore = 2; }
    else if (sheepCount <= 7) { sheepScore = 3; }
    else if (sheepCount >= 8) { sheepScore = 4; }
    return sheepScore;
  }

  export function getBoarScore(boarCount) {
    let boarScore;    
    if (boarCount === 0) { boarScore = -1; }
    else if (boarCount <= 2) { boarScore = 1; }
    else if (boarCount <= 4) { boarScore = 2; }
    else if (boarCount <= 6) { boarScore = 3; }
    else if (boarCount >= 7) { boarScore = 4; }
    return boarScore;
  }

  export function getCattleScore(cattleCount) {
    let cattleScore;    
    if (cattleCount === 0) { cattleScore = -1; }
    else if (cattleCount === 1) { cattleScore = 1; }
    else if (cattleCount <= 3) { cattleScore = 2; }
    else if (cattleCount <= 5) { cattleScore = 3; }
    else if (cattleCount >= 6) { cattleScore = 4; }
    return cattleScore;
  }

  export function getStablesScore(stablesCount) {
    let stablesScore = stablesCount;
    return stablesScore;
  }

  export function getUnusedScore(unusedCount) {
    let unusedScore = -Math.abs(unusedCount);
    return unusedScore;
  }

  export function getClayScore(clayCount) {
    let clayScore = clayCount;     
    return clayScore;
  }

  export function getStoneScore(stoneCount) {
    let stoneScore = stoneCount * 2;
    return stoneScore;
  }

  export function getFamilyScore(familyCount) {
    let familyScore = familyCount * 3;
    return familyScore;
  }

  export function getBonusScore(bonusCount) {
    let bonusScore = bonusCount;
    return bonusScore;
  }

  export  function getCardsScore(cardsCount) {
    let cardsScore = cardsCount;
    return cardsScore;
  }

  export function getBeggingScore(beggingCount) {
    let beggingScore = -Math.abs(beggingCount) * 3;
    return beggingScore;
  }