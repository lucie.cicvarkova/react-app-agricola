import React, { useState } from 'react';
import styled from 'styled-components';
import * as scoreCounters from './scoreCounters.jsx';

const Wrapper = styled.div`
position: relative;
width: 200px;
background-color: white;
border-radius: 7px;
padding-left: 30px;
margin-right: 15px;
`;

const Input = styled.input`
width: 2.5rem;
border-radius: 3px;
border-color: grey;
margin-right: 42px;
color: grey;
padding-left: 0.5rem;
`;

const CountButton = styled.button`
width: 5em;
padding: 3px;
margin-top: 15px;
margin-bottom: 10px;
background-color: white;
border: 2px solid grey;
border-radius: 3px;
font-size: 1em;
color: grey;
cursor: pointer;
`;

const DeleteButton = styled.button`
position: absolute;
top: 5px;
left: 130px;
background-color: white;
border: 2px solid grey;
border-radius: 3px;
color: grey;
margin-left: 2.5em;
cursor: pointer;
.tooltip {
  display: none;
}
:hover .tooltip {
  display: block;
  position: absolute;
  left: 1.5rem;  
  background-color: white;
  border: 1px solid grey;
  border-radius: 0px 10px 10px 10px;
  padding: 0.5rem;
  z-index: 1;
  }
`;


export function PlayerColumn(props) {

  const [values, setValues] = useState({
    fields: 0, pastures: 0, grain: 0, vegetables: 0, sheep: 0, boar: 0, cattle: 0, unused: 0, stables: 0,
    clay: 0, stone: 0, family: 2, cards: 0, bonus: 0, begging: 0
  })

  const [scores, setScores] = useState({
    fieldsScore: -1, pasturesScore: -1, grainScore: -1, vegetablesScore: -1, sheepScore: -1, boarScore: -1,
    cattleScore: -1, unusedScore: 0, stablesScore: 0, clayScore: 0, stoneScore: 0, familyScore: 6,
    cardsScore: 0, bonusScore: 0, beggingScore: 0
  })

  const [totalScore, setTotalScore] = useState(0);



  const handleSubmit = (event) => {
    event.preventDefault();
  }

  const setInput = (event) => {
    setValues(prevValues => {
      return { ...prevValues, [event.target.name]: event.target.value }
    });

    switch (event.target.name) {
      
      case 'fields':
        const actualFieldsScore = scoreCounters.getFieldsScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'fieldsScore': actualFieldsScore }
        });
        break;
      case 'pastures':
        const actualPasturesScore = scoreCounters.getPasturesScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'pasturesScore': actualPasturesScore }
        });
        break;
      case 'grain':
        const actualGrainScore = scoreCounters.getGrainScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'grainScore': actualGrainScore }
        });
        break;
      case 'vegetables':
        const actualVegetableScore = scoreCounters.getVegetablesScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'vegetablesScore': actualVegetableScore }
        });
        break;
      case 'sheep':
        const actualSheepScore = scoreCounters.getSheepScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'sheepScore': actualSheepScore }
        });
        break;
      case 'boar':
        const actualBoarScore = scoreCounters.getBoarScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'boarScore': actualBoarScore }
        });
        break;
      case 'cattle':
        const actualCattleScore = scoreCounters.getCattleScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'cattleScore': actualCattleScore }
        });
        break;
      case 'unused':
        const actualUnusedScore = scoreCounters.getUnusedScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'unusedScore': actualUnusedScore }
        });
        break;
      case 'stables':
        const actualStablesScore = scoreCounters.getStablesScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'stablesScore': actualStablesScore }
        });
        break;
      case 'clay':
        const actualClayScore = scoreCounters.getClayScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'clayScore': actualClayScore }
        });
        break;
      case 'stone':
        const actualStoneScore = scoreCounters.getStoneScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'stoneScore': actualStoneScore }
        });
        break;
      case 'family':
        const actualFamilyScore = scoreCounters.getFamilyScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'familyScore': actualFamilyScore }
        });
        break;
      case 'cards':
        const actualCardsScore = scoreCounters.getCardsScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'cardsScore': actualCardsScore }
        });
        break;
      case 'bonus':
        const actualBonusScore = scoreCounters.getBonusScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'bonusScore': actualBonusScore }
        });
        break;
      case 'begging':
        const actualBeggingScore = scoreCounters.getBeggingScore(parseInt(event.target.value));
        setScores(prevScores => {
          return { ...prevScores, 'beggingScore': actualBeggingScore }
        });
        break;
      default: 
      new Error("Unexpected target name");  
    }
  }


  const countScore = () => {
    setTotalScore(scores.fieldsScore + scores.pasturesScore +
      scores.grainScore + scores.vegetablesScore + scores.sheepScore +
      scores.boarScore + scores.cattleScore + scores.unusedScore + scores.stablesScore +
      scores.clayScore + scores.stoneScore + scores.familyScore + scores.cardsScore + scores.bonusScore
      + scores.beggingScore);
  }

  return (
    <form onSubmit={handleSubmit}>
      <Wrapper>
        <ul>
          <li style={{ fontWeight: 'bold' }}>{props.player}
            <DeleteButton onClick={() => props.deleteFunction(props.player)} className={props.display}>X
                <div className="tooltip">Vymaž kartu hráče</div>
            </DeleteButton>
          </li>
          <li style={{ fontStyle: 'italic' }}>položky &rarr; body</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="fields" max="13" defaultValue={values.fields} onChange={setInput} />{scores.fieldsScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" max="5" name="pastures" defaultValue={values.pastures} onChange={setInput} />{scores.pasturesScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="grain" defaultValue={values.grain} onChange={setInput} />{scores.grainScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="vegetables" defaultValue={values.vegetables} onChange={setInput} />{scores.vegetablesScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="sheep" defaultValue={values.sheep} onChange={setInput} />{scores.sheepScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="boar" defaultValue={values.boar} onChange={setInput} />{scores.boarScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="cattle" defaultValue={values.cattle} onChange={setInput} />{scores.cattleScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" max="13" name="unused" defaultValue={values.unused} onChange={setInput} />{scores.unusedScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" max="4" name="stables" defaultValue={values.stables} onChange={setInput} />{scores.stablesScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="clay" defaultValue={values.clay} onChange={setInput} />{scores.clayScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="stone" defaultValue={values.stone} onChange={setInput} />{scores.stoneScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="2" max="5" name="family" defaultValue={values.family} onChange={setInput} />{scores.familyScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="cards" defaultValue={values.cards} onChange={setInput} />{scores.cardsScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="bonus" defaultValue={values.bonus} onChange={setInput} />{scores.bonusScore}</li>
          <li><Input disabled={props.disableInputs} type="number" min="0" name="begging" defaultValue={values.begging} onChange={setInput} />{scores.beggingScore}</li>
          <li><CountButton disabled={props.disableInputs} onClick={countScore}>spočítej</CountButton></li>
          <li style={{ fontWeight: 'bold' }}>{totalScore}</li>
        </ul>
      </Wrapper>
    </form>
  );
}
