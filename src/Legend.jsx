import React from 'react';
import styled from 'styled-components';

const List = styled.ul`
width: 13rem;
`;


export function Legend() {
    return (
        <List>
    <ul>
        <li>HRÁČI</li>
        <br/>     
        <li>zoraná pole</li>
        <li>pastviny</li>
        <li>obilí</li>
        <li>zelenina</li>
        <li>ovce</li>
        <li>prasata</li>
        <li>krávy</li>
        <li>nevyužitá políčka</li>
        <li>oplocené chlévy</li>
        <li>jílová chýše</li>
        <li>kamenný dům</li>
        <li>členové rodiny</li>
        <li>body za karty</li>
        <li>body navíc</li>
        <li>karty žebrání</li>
        <br/ >
        <br/ >
      <li>BODY CELKEM</li>
          </ul>
          </List>
    );
}