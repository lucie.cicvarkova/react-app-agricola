# Agricola Score App

App made for studying purposes in learning React.  
Counts players´ score in the Agricola board game (https://en.wikipedia.org/wiki/Agricola_(board_game))  

# Run the app on Vercel  
https://react-app-agricola-58as08gnq-luciecicvarkova.vercel.app/

