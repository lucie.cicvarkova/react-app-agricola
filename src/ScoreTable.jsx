import React, { useState } from 'react';
import styled from 'styled-components';
import { Legend } from './Legend';
import { PlayerColumn } from './PlayerColumn';
import './App.css';

const ScoreTableStyle = styled.div`
width: fit-content;
border: 2px solid rgb(118, 109, 101);
border-radius: 3px;
margin: 20px;
padding: 12px;
ul {
  list-style-type: none;  
  padding-left: 0;
  padding-right: 10px;
}
`;

const H1 = styled.div`
width: 50%;
margin: auto;
text-align: center;
font-size: 2em;
font-weight: lighter;
letter-spacing: 0.2em;
`;

const H2 = styled.div`
width: 60%;
margin: 0 auto 0.5em;
text-align: center;
font-size: 1.2em;
font-weight: lighter;
letter-spacing: 0.1em;
`;

const InnerDiv = styled.div`
background-color: rgb(249, 245, 239);
padding: 15px 0px 15px 5px;
`;

const Header = styled.div`
width: 95%;
margin: auto;
`;

const Columns = styled.div`
width: 95%;
margin: auto;
display: flex;
flex-direction: row;
justify-content: flex-start;
`;

const Input = styled.input`
width: 150px;
height: 30px;
padding: 5px;
margin: 5px;
margin-left: 3em;
border-color: rgb(118, 109, 101) ;
border-radius: 3px;
color: grey;
`;

const Button = styled.button`
width: 5em;
padding: 3px;
margin: 5px;
background-color: white;
border: 2px solid rgb(118, 109, 101);
border-radius: 3px;
font-size: 1em;
color: rgb(118, 109, 101);
`;


export function ScoreTable() {
 
  const [playerName, setPlayerName] = useState('');  //pro vepisování jména hráče do inputu
  const [playerList, setPlayerList] = useState([]);  //pro vykreslování karet hráčů (1 hráč = 1 karta)
                                                 

  const handleSubmit = (e) => {
    e.preventDefault();
  }

  const handleInputChange = (e) => {
    setPlayerName(e.target.value);
  }

  const saveInputChange = (e) => {
    e.preventDefault();  
    setPlayerList(prevValues => [...prevValues, playerName]);
    setPlayerName('');
  }

  const deletePlayer = (playerName) => {
    const newPlayerList = playerList.filter((player) => player !== playerName);    
    setPlayerList(newPlayerList) ;
  }
   

     return (
      <>
        <ScoreTableStyle>
        <H1>AGRICOLA</H1>
        <H2> VYHODNOCENÍ HRY</H2>
          <InnerDiv>
            <Header>
              <form onSubmit={handleSubmit}>
                NOVÝ HRÁČ:
            <Input type="text" name='playerName' onChange={handleInputChange} value={playerName} />
                <Button type="submit" onClick={saveInputChange}>ulož</Button>
              </form>           
            </Header>
            <Columns>
              <Legend />
              {
               (playerList.length === 0)
                 ? <PlayerColumn player={'jméno hráče'} disableInputs={true} display={'delete-button-hidden'} />
                 : playerList.map(player => (
                   <PlayerColumn player={player} deleteFunction={deletePlayer} />
                   ))
             }
            </Columns>
          </InnerDiv>
        </ScoreTableStyle>
      </>
    );
}
